require 'curses'
include Curses

# setpos(y, x)

Curses.init_screen
Curses.crmode
Curses.start_color
Curses.nl
Curses.noecho
Curses.curs_set(0)

COLORS = [COLOR_GREEN, COLOR_RED, COLOR_YELLOW, COLOR_MAGENTA, COLOR_CYAN, COLOR_BLUE]

COLORS.each do |color|
  Curses.init_pair(color, color, COLOR_BLACK)
end

def draw_text_on_center(y, text)
  attron(color_pair(Curses::COLOR_BLUE)) do
    setpos(y, Curses::cols / 2 - (text.size / 2.0).round(half: :even))
    addstr(text)
  end
end

def draw_star(y, x)
  attron(color_pair(Curses::COLOR_BLUE) | Curses::A_BOLD) {
    setpos(y, x); addstr("0")
    setpos(y - 1, x); addstr("|")
    setpos(y - 2, x); addstr("|")
    setpos(y + 1, x); addstr("|")
    setpos(y + 2, x); addstr("|")
    setpos(y, x - 1); addstr("=")
    setpos(y, x - 2); addstr("-")
    setpos(y, x - 3); addstr("-")
    setpos(y, x + 1); addstr("=")
    setpos(y, x + 2); addstr("-")
    setpos(y, x + 3); addstr("-")
    setpos(y + 1, x - 1); addstr("/")
    setpos(y - 1, x - 1); addstr("\\")
    setpos(y + 1, x + 1); addstr("\\")
    setpos(y - 1, x + 1); addstr("/")
  }
end

def sections_border(header_indent, section_size)
  sections_count = (Curses.lines - header_indent) / section_size - 1
  coordinates = []

  sections_count.times do |i|
    line = header_indent + (section_size * i) + 1
    indent = (2 * section_size - 4) * i + 1

    return coordinates if Curses.cols / 2 < indent + section_size + 2

    section_size.times do |i|
      coordinates << { y: line + i, x: Curses.cols / 2 - indent - i * 2, char: "/" }
      coordinates << { y: line + i, x: Curses.cols / 2 + indent + i * 2, char: "\\" }
    end
  end

  return coordinates
end

def draw_sections_border(coordinates)
  coordinates.each do |c|
    attron(color_pair(COLOR_GREEN)) do
      setpos(c[:y], c[:x])
      addstr(c[:char])
    end
  end
end


def sections_ornament(header_indent, section_size)
  sections_count = (Curses.lines - header_indent) / section_size - 1
  coordinates = []

  sections_count.times do |i|
    line = header_indent + (section_size * i) + 1
    indent = (2 * section_size - 4) * i + 1

    return coordinates if Curses.cols / 2 < indent + section_size + 2

    section_size.times do |i|
      range = (Curses.cols / 2 - indent + 1 - i * 2 ... Curses.cols / 2 + indent + i * 2)
      ornaments = range.to_a.zip(Array.new(range.size) { rand(0..range.size/2) })
      ornaments.each do |gift|
        if gift[1] == 0
          coordinates << { y: line + i, x: gift[0], char: "O" }
        end
      end
    end

  end

  return coordinates
end

def draw_sections_ornament(coordinates)
  coordinates.each do |c|
    attron(color_pair(COLORS.sample) | [Curses::A_BOLD, Curses::A_NORMAL].sample) do
      setpos(c[:y], c[:x])
      addstr(c[:char])
    end
  end
end

header_indent = 5
section_size = 4
sections_count = (Curses.lines - header_indent) / section_size - 1

begin
  ornament = sections_ornament(header_indent, section_size)
  border = sections_border(header_indent, section_size)

  draw_sections_border(border)

  while true
    draw_sections_ornament(ornament)
    draw_text_on_center(1, "Happy New 2018 Year!")
    draw_star(header_indent, Curses.cols / 2)

    Curses.refresh
    sleep(0.5)
  end
ensure
  Curses::close_screen
end
